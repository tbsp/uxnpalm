
#ifndef UXNEMU_H_
#define UXNEMU_H_

#ifndef __ARM__

	//globals (8 slots maximum, each stores a void*, zero-inited at app start)

	#define NUM_GLOBALS_SLOTS		8

	register void** a5 asm("a5");

	static inline void** globalsSlotPtr(UInt8 slotID)	//[0] is reserved
	{
		if (!slotID || slotID > NUM_GLOBALS_SLOTS)
			return NULL;

		return a5 + slotID;
	}

	static inline void* globalsSlotVal(UInt8 slotID)	//[0] is reserved
	{
		if (!slotID || slotID > NUM_GLOBALS_SLOTS)
			return NULL;

		return a5[slotID];
	}

	#define GLOBALS_SLOT_SHARED_VARS	1
	#define GLOBALS_SLOT_UXN_CORE		2
	#define GLOBALS_SLOT_SCREEN_DEVICE	3

#endif

typedef struct SharedVariables {
	UInt32 lastKeyState; // Used to track button down/up states
	int timeInterval;
	int timeForNextFrame;
	Uint32 ticksOfLastFrame; // Used to show ticks per frame
	char **romNames; // Accessing this without caching it is hard (gluelib won't work, ALLOW_ACCESS_TO_INTERNALS_OF_LISTS hack is ugly, so cache it!)
	int numROMs;
} SharedVariables;

#endif /* UXNEMU_H_ */