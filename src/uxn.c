#include <PalmOS.h>
#include "uxn.h"

/*
Copyright (u) 2022-2023 Devine Lu Linvega, Andrew Alderwick, Andrew Richards

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

/* Registers
[ Z ][ Y ][ X ][ L ][ N ][ T ] <
[ . ][ . ][ . ][   H2   ][ . ] <
[   L2   ][   N2   ][   T2   ] <
*/

#define UNROLLED_CORE

#define DEO(p, value) { u->dev[p] = value; if((deo_mask[p >> 4] >> (p & 0xf)) & 0x1) emu_deo(u, p); }
#define DEI(p) ((dei_mask[(p) >> 4] >> ((p) & 0xf)) & 0x1 ? emu_dei(u, (p)) : u->dev[(p)])

#ifndef UNROLLED_CORE
	#define T *(s->dat + s->ptr)
	#define N *(s->dat + (Uint8)(s->ptr - 1))
	#define L *(s->dat + (Uint8)(s->ptr - 2))
	#define X *(s->dat + (Uint8)(s->ptr - 3))
	#define Y *(s->dat + (Uint8)(s->ptr - 4))
	#define Z *(s->dat + (Uint8)(s->ptr - 5))
	#define T2 (N << 8 | T)
	#define H2 (L << 8 | N)
	#define N2 (X << 8 | L)
	#define L2 (Z << 8 | Y)
	#define T2_(v) { q = (v); T = q; N = q >> 8; }
	#define N2_(v) { q = (v); L = q; X = q >> 8; }
	#define L2_(v) { q = (v); Y = q; Z = q >> 8; }
	#define FLIP      { s = ins & 0x40 ? &u->wst : &u->rst; }
	#define SHIFT(y)  { s->ptr += (y); }
	#define SET(x, y) { SHIFT((ins & 0x80) ? x + y : y) }
#else
	#define T(s) *(s->dat + s->ptr)
	#define N(s) *(s->dat + (Uint8)(s->ptr - 1))
	#define L(s) *(s->dat + (Uint8)(s->ptr - 2))
	#define X(s) *(s->dat + (Uint8)(s->ptr - 3))
	#define Y(s) *(s->dat + (Uint8)(s->ptr - 4))
	#define Z(s) *(s->dat + (Uint8)(s->ptr - 5))
	#define T2(s) (N(s) << 8 | T(s))
	#define H2(s) (L(s) << 8 | N(s))
	#define N2(s) (X(s) << 8 | L(s))
	#define L2(s) (Z(s) << 8 | Y(s))
	#define T2_(s, v) { q = (v); T(s) = q; N(s) = q >> 8; }
	#define N2_(s, v) { q = (v); L(s) = q; X(s) = q >> 8; }
	#define L2_(s, v) { q = (v); Y(s) = q; Z(s) = q >> 8; }
	#define SHIFT(s, y) { s->ptr += (y); }
#endif

int
uxn_eval(Uxn *u, Uint16 pc)
{
	Uint16 t, n, l, q;
	Uint8 *ram = u->ram, *rr;
	#ifdef UNROLLED_CORE
		Stack *w = &u->wst, *r = &u->rst;
	#endif
	if(!pc || u->dev[0x0f]) return 0;
	for(;;) {
		int ins = ram[pc++];
		#ifndef UNROLLED_CORE
			Stack *s = ins & 0x40 ? &u->rst : &u->wst;
			switch(ins & 0x3f) {
			/* IMM */
			case 0x00: case 0x20:
				switch(ins) {
				case 0x00: /* BRK  */                       return 1;
				case 0x20: /* JCI  */ t=T;        SHIFT(-1) if(!t) { pc += 2; break; } /* fall-through */
				case 0x40: /* JMI  */                       rr = ram + pc; pc += 2 + PEEK2(rr); break;
				case 0x60: /* JSI  */             SHIFT( 2) rr = ram + pc; pc += 2; T2_(pc); pc += PEEK2(rr); break;
				case 0x80: /* LIT  */ case 0xc0:  SHIFT( 1) T = ram[pc++]; break;
				case 0xa0: /* LIT2 */ case 0xe0:  SHIFT( 2) N = ram[pc++]; T = ram[pc++]; break;
				} break;
			/* ALU */
			case 0x01: /* INC  */ t=T;            SET(1, 0) T = t + 1; break;
			case 0x21: /* INC2 */ t=T2;           SET(2, 0) T2_(t + 1) break;
			case 0x02: /* POP  */                 SET(1,-1) break;
			case 0x22: /* POP2 */                 SET(2,-2) break;
			case 0x03: /* NIP  */ t=T;            SET(2,-1) T = t; break;
			case 0x23: /* NIP2 */ t=T2;           SET(4,-2) T2_(t) break;
			case 0x04: /* SWP  */ t=T;n=N;        SET(2, 0) T = n; N = t; break;
			case 0x24: /* SWP2 */ t=T2;n=N2;      SET(4, 0) T2_(n) N2_(t) break;
			case 0x05: /* ROT  */ t=T;n=N;l=L;    SET(3, 0) T = l; N = t; L = n; break;
			case 0x25: /* ROT2 */ t=T2;n=N2;l=L2; SET(6, 0) T2_(l) N2_(t) L2_(n) break;
			case 0x06: /* DUP  */ t=T;            SET(1, 1) T = t; N = t; break;
			case 0x26: /* DUP2 */ t=T2;           SET(2, 2) T2_(t) N2_(t) break;
			case 0x07: /* OVR  */ t=T;n=N;        SET(2, 1) T = n; N = t; L = n; break;
			case 0x27: /* OVR2 */ t=T2;n=N2;      SET(4, 2) T2_(n) N2_(t) L2_(n) break;
			case 0x08: /* EQU  */ t=T;n=N;        SET(2,-1) T = n == t; break;
			case 0x28: /* EQU2 */ t=T2;n=N2;      SET(4,-3) T = n == t; break;
			case 0x09: /* NEQ  */ t=T;n=N;        SET(2,-1) T = n != t; break;
			case 0x29: /* NEQ2 */ t=T2;n=N2;      SET(4,-3) T = n != t; break;
			case 0x0a: /* GTH  */ t=T;n=N;        SET(2,-1) T = n > t; break;
			case 0x2a: /* GTH2 */ t=T2;n=N2;      SET(4,-3) T = n > t; break;
			case 0x0b: /* LTH  */ t=T;n=N;        SET(2,-1) T = n < t; break;
			case 0x2b: /* LTH2 */ t=T2;n=N2;      SET(4,-3) T = n < t; break;
			case 0x0c: /* JMP  */ t=T;            SET(1,-1) pc += (Sint8)t; break;
			case 0x2c: /* JMP2 */ t=T2;           SET(2,-2) pc = t; break;
			case 0x0d: /* JCN  */ t=T;n=N;        SET(2,-2) if(n) pc += (Sint8)t; break;
			case 0x2d: /* JCN2 */ t=T2;n=L;       SET(3,-3) if(n) pc = t; break;
			case 0x0e: /* JSR  */ t=T;            SET(1,-1) FLIP SHIFT(2) T2_(pc) pc += (Sint8)t; break;
			case 0x2e: /* JSR2 */ t=T2;           SET(2,-2) FLIP SHIFT(2) T2_(pc) pc = t; break;
			case 0x0f: /* STH  */ t=T;            SET(1,-1) FLIP SHIFT(1) T = t; break;
			case 0x2f: /* STH2 */ t=T2;           SET(2,-2) FLIP SHIFT(2) T2_(t) break;
			case 0x10: /* LDZ  */ t=T;            SET(1, 0) T = ram[t]; break;
			case 0x30: /* LDZ2 */ t=T;            SET(1, 1) N = ram[t++]; T = ram[(Uint8)t]; break;
			case 0x11: /* STZ  */ t=T;n=N;        SET(2,-2) ram[t] = n; break;
			case 0x31: /* STZ2 */ t=T;n=H2;       SET(3,-3) ram[t++] = n >> 8; ram[(Uint8)t] = n; break;
			case 0x12: /* LDR  */ t=T;            SET(1, 0) q = pc + (Sint8)t; T = ram[q]; break;
			case 0x32: /* LDR2 */ t=T;            SET(1, 1) q = pc + (Sint8)t; N = ram[q++]; T = ram[q]; break;
			case 0x13: /* STR  */ t=T;n=N;        SET(2,-2) q = pc + (Sint8)t; ram[q] = n; break;
			case 0x33: /* STR2 */ t=T;n=H2;       SET(3,-3) q = pc + (Sint8)t; ram[q++] = n >> 8; ram[q] = n; break;
			case 0x14: /* LDA  */ t=T2;           SET(2,-1) T = ram[t]; break;
			case 0x34: /* LDA2 */ t=T2;           SET(2, 0) N = ram[t++]; T = ram[t]; break;
			case 0x15: /* STA  */ t=T2;n=L;       SET(3,-3) ram[t] = n; break;
			case 0x35: /* STA2 */ t=T2;n=N2;      SET(4,-4) ram[t++] = n >> 8; ram[t] = n; break;
			case 0x16: /* DEI  */ t=T;            SET(1, 0) T = DEI(t); break;
			case 0x36: /* DEI2 */ t=T;            SET(1, 1) N = DEI(t); T = DEI((t + 1)); break;
			case 0x17: /* DEO  */ t=T;n=N;        SET(2,-2) DEO(t, n); break;
			case 0x37: /* DEO2 */ t=T;n=N;l=L;    SET(3,-3) DEO(t, l); DEO((t + 1), n); break;
			case 0x18: /* ADD  */ t=T;n=N;        SET(2,-1) T = n + t; break;
			case 0x38: /* ADD2 */ t=T2;n=N2;      SET(4,-2) T2_(n + t) break;
			case 0x19: /* SUB  */ t=T;n=N;        SET(2,-1) T = n - t; break;
			case 0x39: /* SUB2 */ t=T2;n=N2;      SET(4,-2) T2_(n - t) break;
			case 0x1a: /* MUL  */ t=T;n=N;        SET(2,-1) T = n * t; break;
			case 0x3a: /* MUL2 */ t=T2;n=N2;      SET(4,-2) T2_(n * t) break;
			case 0x1b: /* DIV  */ t=T;n=N;        SET(2,-1) T = t ? n / t : 0; break;
			case 0x3b: /* DIV2 */ t=T2;n=N2;      SET(4,-2) T2_(t ? n / t : 0) break;
			case 0x1c: /* AND  */ t=T;n=N;        SET(2,-1) T = n & t; break;
			case 0x3c: /* AND2 */ t=T2;n=N2;      SET(4,-2) T2_(n & t) break;
			case 0x1d: /* ORA  */ t=T;n=N;        SET(2,-1) T = n | t; break;
			case 0x3d: /* ORA2 */ t=T2;n=N2;      SET(4,-2) T2_(n | t) break;
			case 0x1e: /* EOR  */ t=T;n=N;        SET(2,-1) T = n ^ t; break;
			case 0x3e: /* EOR2 */ t=T2;n=N2;      SET(4,-2) T2_(n ^ t) break;
			case 0x1f: /* SFT  */ t=T;n=N;        SET(2,-1) T = n >> (t & 0xf) << (t >> 4); break;
			case 0x3f: /* SFT2 */ t=T;n=H2;       SET(3,-1) T2_(n >> (t & 0xf) << (t >> 4)) break;
			}
		#else
			switch(ins) {
				case 0x00: /* BRK    */	return 1;
				case 0x01: /* INC    */ T(w) += 1; break;
				case 0x02: /* POP    */ SHIFT(w, -1); break;
				case 0x03: /* NIP    */ t=T(w); SHIFT(w, -1); T(w)=t; break;
				case 0x04: /* SWP    */ t=T(w); n=N(w); T(w)=n; N(w)=t; break;
				case 0x05: /* ROT    */ t=T(w); n=N(w); l=L(w); T(w)=l; N(w)=t; L(w)=n; break;
				case 0x06: /* DUP    */ t=T(w); SHIFT(w, 1) T(w)=t; break;
				case 0x07: /* OVR    */ t=T(w); n=N(w); SHIFT(w, 1) T(w) = n; N(w)=t; L(w)=n; break;
				case 0x08: /* EQU    */ N(w) = N(w) == T(w); SHIFT(w, -1) break;
				case 0x09: /* NEQ    */ N(w) = N(w) != T(w); SHIFT(w, -1) break;
				case 0x0a: /* GTH    */ N(w) = N(w) >  T(w); SHIFT(w, -1) break;
				case 0x0b: /* LTH    */ N(w) = N(w) <  T(w); SHIFT(w, -1) break;
				case 0x0c: /* JMP    */ t=T(w); SHIFT(w, -1) pc += (Sint8)t; break;
				case 0x0d: /* JCN    */ t=T(w); n=N(w); SHIFT(w, -2) if(n) pc += (Sint8)t; break;
				case 0x0e: /* JSR    */ t=T(w); SHIFT(w, -1) SHIFT(r, 2) T2_(r, pc) pc += (Sint8)t; break;
				case 0x0f: /* STH    */ t=T(w); SHIFT(w, -1) SHIFT(r, 1) T(r) = t; break;
				case 0x10: /* LDZ    */ t=T(w); T(w) = ram[t]; break;
				case 0x11: /* STZ    */ t=T(w); n=N(w); SHIFT(w, -2) ram[t] = n; break;
				case 0x12: /* LDR    */ t=T(w); q = pc + (Sint8)t; T(w) = ram[q]; break;
				case 0x13: /* STR    */ t=T(w); n=N(w); SHIFT(w, -2) q = pc + (Sint8)t; ram[q] = n; break;
				case 0x14: /* LDA    */ t=T2(w); SHIFT(w, -1) T(w) = ram[t]; break;
				case 0x15: /* STA    */ t=T2(w); n=L(w); SHIFT(w, -3) ram[t] = n; break;
				case 0x16: /* DEI    */ t=T(w); T(w) = DEI(t); break;
				case 0x17: /* DEO    */ t=T(w); n=N(w); SHIFT(w, -2) DEO(t, n); break;
				case 0x18: /* ADD    */ t=T(w); n=N(w); SHIFT(w, -1) T(w) = n + t; break;
				case 0x19: /* SUB    */ t=T(w); n=N(w); SHIFT(w, -1) T(w) = n - t; break;
				case 0x1a: /* MUL    */ t=T(w); n=N(w); SHIFT(w, -1) T(w) = n * t; break;
				case 0x1b: /* DIV    */ t=T(w); n=N(w); SHIFT(w, -1) T(w) = t ? n / t : 0; break;
				case 0x1c: /* AND    */ t=T(w); n=N(w); SHIFT(w, -1) T(w) = n & t; break;
				case 0x1d: /* ORA    */ t=T(w); n=N(w); SHIFT(w, -1) T(w) = n | t; break;
				case 0x1e: /* EOR    */ t=T(w); n=N(w); SHIFT(w, -1) T(w) = n ^ t; break;
				case 0x1f: /* SFT    */ t=T(w); n=N(w); SHIFT(w, -1) T(w) = n >> (t & 0xf) << (t >> 4); break;
				case 0x20: /* JCI    */ t=T(w); SHIFT(w, -1) if(!t) { pc += 2; break; } rr = ram + pc; pc += 2 + PEEK2(rr); break;
				case 0x21: /* INC2   */ t=T2(w); T2_(w, t + 1) break;
				case 0x22: /* POP2   */ SHIFT(w, -2) break;
				case 0x23: /* NIP2   */ t=T2(w); SHIFT(w, -2) T2_(w, t) break;
				case 0x24: /* SWP2   */ t=T2(w); n=N2(w); T2_(w, n) N2_(w, t) break;
				case 0x25: /* ROT2   */ t=T2(w); n=N2(w); l=L2(w); T2_(w, l) N2_(w, t) L2_(w, n) break;
				case 0x26: /* DUP2   */ t=T2(w); SHIFT(w, 2) T2_(w, t) break;
				case 0x27: /* OVR2   */ t=T2(w); n=N2(w); SHIFT(w, 2) T2_(w, n) N2_(w, t) L2_(w, n) break;
				case 0x28: /* EQU2   */ t=T2(w); n=N2(w); SHIFT(w, -3) T(w) = n == t; break; // opt like EQU/etc
				case 0x29: /* NEQ2   */ t=T2(w); n=N2(w); SHIFT(w, -3) T(w) = n != t; break;
				case 0x2a: /* GTH2   */ t=T2(w); n=N2(w); SHIFT(w, -3) T(w) = n >  t; break;
				case 0x2b: /* LTH2   */ t=T2(w); n=N2(w); SHIFT(w, -3) T(w) = n <  t; break;
				case 0x2c: /* JMP2   */ t=T2(w); SHIFT(w, -2) pc = t; break;
				case 0x2d: /* JCN2   */ t=T2(w); n=L(w); SHIFT(w, -3) if(n) pc = t; break;
				case 0x2e: /* JSR2   */ t=T2(w); SHIFT(w, -2) SHIFT(r, 2) T2_(r, pc) pc = t; break;
				case 0x2f: /* STH2   */ t=T2(w); SHIFT(w,-2) SHIFT(r, 2) T2_(r, t) break;
				case 0x30: /* LDZ2   */ t=T(w); SHIFT(w, 1) N(w) = ram[t++]; T(w) = ram[(Uint8)t]; break;
				case 0x31: /* STZ2   */ t=T(w); n=H2(w); SHIFT(w, -3) ram[t++] = n >> 8; ram[(Uint8)t] = n; break;
				case 0x32: /* LDR2   */ t=T(w); SHIFT(w, 1) q = pc + (Sint8)t; N(w) = ram[q++]; T(w) = ram[q]; break;
				case 0x33: /* STR2   */ t=T(w); n=H2(w); SHIFT(w, -3) q = pc + (Sint8)t; ram[q++] = n >> 8; ram[q] = n; break;
				case 0x34: /* LDA2   */ t=T2(w); N(w) = ram[t++]; T(w) = ram[t]; break;
				case 0x35: /* STA2   */ t=T2(w); n=N2(w); SHIFT(w, -4) ram[t++] = n >> 8; ram[t] = n; break;
				case 0x36: /* DEI2   */ t=T(w); SHIFT(w, 1) N(w) = DEI(t); T(w) = DEI((t + 1)); break;
				case 0x37: /* DEO2   */ t=T(w); n=N(w); l=L(w); SHIFT(w, -3) DEO(t, l); DEO((t + 1), n); break;
				case 0x38: /* ADD2   */ t=T2(w); n=N2(w); SHIFT(w, -2) T2_(w, n + t) break;
				case 0x39: /* SUB2   */ t=T2(w); n=N2(w); SHIFT(w, -2) T2_(w, n - t) break;
				case 0x3a: /* MUL2   */ t=T2(w); n=N2(w); SHIFT(w, -2) T2_(w, n * t) break;
				case 0x3b: /* DIV2   */ t=T2(w); n=N2(w); SHIFT(w, -2) T2_(w, t ? n / t : 0) break;
				case 0x3c: /* AND2   */ t=T2(w); n=N2(w); SHIFT(w, -2) T2_(w, n & t) break;
				case 0x3d: /* ORA2   */ t=T2(w); n=N2(w); SHIFT(w, -2) T2_(w, n | t) break;
				case 0x3e: /* EOR2   */ t=T2(w); n=N2(w); SHIFT(w, -2) T2_(w, n ^ t) break;
				case 0x3f: /* SFT2   */ t=T(w); n=H2(w); SHIFT(w, -1) T2_(w, n >> (t & 0xf) << (t >> 4)) break;
				case 0x40: /* JMI    */ rr = ram + pc; pc += 2 + PEEK2(rr); break;
				case 0x41: /* INCr   */ T(r) += 1; break;
				case 0x42: /* POPr   */ SHIFT(r, -1) break;
				case 0x43: /* NIPr   */ t=T(r); SHIFT(r, -1); T(r)=t; break;
				case 0x44: /* SWPr   */ t=T(r); n=N(r); T(r)=n; N(r)=t; break;
				case 0x45: /* ROTr   */ t=T(r); n=N(r); l=L(r); T(r)=l; N(r)=t; L(r)=n; break;
				case 0x46: /* DUPr   */ t=T(r); SHIFT(r, 1) T(r)=t; break;
				case 0x47: /* OVRr   */ t=T(r); n=N(r); SHIFT(r, 1) T(r) = n; N(r)=t; L(r)=n; break;
				case 0x48: /* EQUr   */ N(r) = N(r) == T(r); SHIFT(r, -1) break;
				case 0x49: /* NEQr   */ N(r) = N(r) != T(r); SHIFT(r, -1) break;
				case 0x4a: /* GTHr   */ N(r) = N(r) >  T(r); SHIFT(r, -1) break;
				case 0x4b: /* LTHr   */ N(r) = N(r) <  T(r); SHIFT(r, -1) break;
				case 0x4c: /* JMPr   */ t=T(r); SHIFT(r, -1) pc += (Sint8)t; break;
				case 0x4d: /* JCNr   */ t=T(r); n=N(r); SHIFT(r, -2) if(n) pc += (Sint8)t; break;
				case 0x4e: /* JSRr   */ t=T(r); SHIFT(r, -1) SHIFT(w, 2) T2_(w, pc) pc += (Sint8)t; break;
				case 0x4f: /* STHr   */ t=T(r); SHIFT(r, -1) SHIFT(w, 1) T(w) = t; break;
				case 0x50: /* LDZr   */ t=T(r); T(r) = ram[t]; break;
				case 0x51: /* STZr   */ t=T(r); n=N(r); SHIFT(r, -2) ram[t] = n; break;
				case 0x52: /* LDRr   */ t=T(r); q = pc + (Sint8)t; T(r) = ram[q]; break;
				case 0x53: /* STRr   */ t=T(r); n=N(r); SHIFT(r, -2) q = pc + (Sint8)t; ram[q] = n; break;
				case 0x54: /* LDAr   */ t=T2(r); SHIFT(r, -1) T(r) = ram[t]; break;
				case 0x55: /* STAr   */ t=T2(r); n=L(r); SHIFT(r, -3) ram[t] = n; break;
				case 0x56: /* DEIr   */ t=T(r); T(r) = DEI(t); break;
				case 0x57: /* DEOr   */ t=T(r); n=N(r); SHIFT(r, -2) DEO(t, n); break;
				case 0x58: /* ADDr   */ t=T(r); n=N(r); SHIFT(r, -1) T(r) = n + t; break;
				case 0x59: /* SUBr   */ t=T(r); n=N(r); SHIFT(r, -1) T(r) = n - t; break;
				case 0x5a: /* MULr   */ t=T(r); n=N(r); SHIFT(r, -1) T(r) = n * t; break;
				case 0x5b: /* DIVr   */ t=T(r); n=N(r); SHIFT(r, -1) T(r) = t ? n / t : 0; break;
				case 0x5c: /* ANDr   */ t=T(r); n=N(r); SHIFT(r, -1) T(r) = n & t; break;
				case 0x5d: /* ORAr   */ t=T(r); n=N(r); SHIFT(r, -1) T(r) = n | t; break;
				case 0x5e: /* EORr   */ t=T(r); n=N(r); SHIFT(r, -1) T(r) = n ^ t; break;
				case 0x5f: /* SFTr   */ t=T(r); n=N(r); SHIFT(r, -1) T(r) = n >> (t & 0xf) << (t >> 4); break;
				case 0x60: /* JSI    */ SHIFT(r, 2) rr = ram + pc; pc += 2; T2_(r, pc); pc += PEEK2(rr); break;
				case 0x61: /* INC2r  */ t=T2(r); T2_(r, t + 1) break;
				case 0x62: /* POP2r  */ SHIFT(r, -2) break;
				case 0x63: /* NIP2r  */ t=T2(r); SHIFT(r, -2) T2_(w, t) break;
				case 0x64: /* SWP2r  */ t=T2(r); n=N2(r); T2_(r, n) N2_(r, t) break;
				case 0x65: /* ROT2r  */ t=T2(r); n=N2(r); l=L2(r); T2_(r, l) N2_(r, t) L2_(r, n) break;
				case 0x66: /* DUP2r  */ t=T2(r); SHIFT(r, 2) T2_(r, t) break;
				case 0x67: /* OVR2r  */ t=T2(r); n=N2(r); SHIFT(r, 2) T2_(r, n) N2_(r, t) L2_(r, n) break;
				case 0x68: /* EQU2r  */ t=T2(r); n=N2(r); SHIFT(r, -3) T(r) = n == t; break; // opt like EQU/etc
				case 0x69: /* NEQ2r  */ t=T2(r); n=N2(r); SHIFT(r, -3) T(r) = n != t; break;
				case 0x6a: /* GTH2r  */ t=T2(r); n=N2(r); SHIFT(r, -3) T(r) = n >  t; break;
				case 0x6b: /* LTH2r  */ t=T2(r); n=N2(r); SHIFT(r, -3) T(r) = n <  t; break;
				case 0x6c: /* JMP2r  */ t=T2(r); SHIFT(r, -2) pc = t; break;
				case 0x6d: /* JCN2r  */ t=T2(r); n=L(r); SHIFT(r, -3) if(n) pc = t; break;
				case 0x6e: /* JSR2r  */ t=T2(r); SHIFT(r, -2) SHIFT(w, 2) T2_(w, pc) pc = t; break;
				case 0x6f: /* STH2r  */ t=T2(r); SHIFT(r,-2) SHIFT(w, 2) T2_(w, t) break;
				case 0x70: /* LDZ2r  */ t=T(r); SHIFT(r, 1) N(r) = ram[t++]; T(r) = ram[(Uint8)t]; break;
				case 0x71: /* STZ2r  */ t=T(r); n=H2(r); SHIFT(r, -3) ram[t++] = n >> 8; ram[(Uint8)t] = n; break;
				case 0x72: /* LDR2r  */ t=T(r); SHIFT(r, 1) q = pc + (Sint8)t; N(r) = ram[q++]; T(r) = ram[q]; break;
				case 0x73: /* STR2r  */ t=T(r); n=H2(r); SHIFT(r, -3) q = pc + (Sint8)t; ram[q++] = n >> 8; ram[q] = n; break;
				case 0x74: /* LDA2r  */ t=T2(r); N(r) = ram[t++]; T(r) = ram[t]; break;
				case 0x75: /* STA2r  */ t=T2(r); n=N2(r); SHIFT(r, -4) ram[t++] = n >> 8; ram[t] = n; break;
				case 0x76: /* DEI2r  */ t=T(r); SHIFT(r, 1) N(r) = DEI(t); T(r) = DEI((t + 1)); break;
				case 0x77: /* DEO2r  */ t=T(r); n=N(r); l=L(r); SHIFT(r, -3) DEO(t, l); DEO((t + 1), n); break;
				case 0x78: /* ADD2r  */ t=T2(r); n=N2(r); SHIFT(r, -2) T2_(r, n + t) break;
				case 0x79: /* SUB2r  */ t=T2(r); n=N2(r); SHIFT(r, -2) T2_(r, n - t) break;
				case 0x7a: /* MUL2r  */ t=T2(r); n=N2(r); SHIFT(r, -2) T2_(r, n * t) break;
				case 0x7b: /* DIV2r  */ t=T2(r); n=N2(r); SHIFT(r, -2) T2_(r, t ? n / t : 0) break;
				case 0x7c: /* AND2r  */ t=T2(r); n=N2(r); SHIFT(r, -2) T2_(r, n & t) break;
				case 0x7d: /* ORA2r  */ t=T2(r); n=N2(r); SHIFT(r, -2) T2_(r, n | t) break;
				case 0x7e: /* EOR2r  */ t=T2(r); n=N2(r); SHIFT(r, -2) T2_(r, n ^ t) break;
				case 0x7f: /* SFT2r  */ t=T(r); n=H2(r); SHIFT(r, -1) T2_(r, n >> (t & 0xf) << (t >> 4)) break;
				case 0x80: /* LIT    */ SHIFT(w, 1) T(w) = ram[pc++]; break;
				case 0x81: /* INCk   */ t=T(w); SHIFT(w, 1) T(w) = t + 1; break;
				case 0x82: /* POPk   */ break;
				case 0x83: /* NIPk   */ t=T(w); SHIFT(w, 1) T(w) = t; break;
				case 0x84: /* SWPk   */ t=T(w); n=N(w); SHIFT(w, 2) T(w)=n; N(w)=t; break;
				case 0x85: /* ROTk   */ t=T(w); n=N(w); l=L(w); SHIFT(w, 3) T(w)=l; N(w)=t; L(w)=n; break;
				case 0x86: /* DUPk   */ t=T(w); SHIFT(w, 2) T(w)=t; N(w)=t; break;
				case 0x87: /* OVRk   */ t=T(w); n=N(w); SHIFT(w, 3) T(w)=n; N(w)=t; L(w)=n; break;
				case 0x88: /* EQUk   */ t=T(w); n=N(w); SHIFT(w, 1) T(w) = n == t; break;
				case 0x89: /* NEQk   */ t=T(w); n=N(w); SHIFT(w, 1) T(w) = n != t; break;
				case 0x8a: /* GTHk   */ t=T(w); n=N(w); SHIFT(w, 1) T(w) = n >  t; break;
				case 0x8b: /* LTHk   */ t=T(w); n=N(w); SHIFT(w, 1) T(w) = n <  t; break;
				case 0x8c: /* JMPk   */ t=T(w); pc += (Sint8)t; break;
				case 0x8d: /* JCNk   */ t=T(w); n=N(w); if(n) pc += (Sint8)t; break;
				case 0x8e: /* JSRk   */ t=T(w); SHIFT(r, 2) T2_(r, pc) pc += (Sint8)t; break;
				case 0x8f: /* STHk   */ t=T(w); SHIFT(r, 1) T(r)=t; break;
				case 0x90: /* LDZk   */ t=T(w); SHIFT(w, 1) T(w) = ram[t]; break;
				case 0x91: /* STZk   */ t=T(w); n=N(w); ram[t] = n; break;
				case 0x92: /* LDRk   */ t=T(w); SHIFT(w, 1) q = pc + (Sint8)t; T(w) = ram[q]; break;
				case 0x93: /* STRk   */ t=T(w); n=N(w); q = pc + (Sint8)t; ram[q] = n; break;
				case 0x94: /* LDAk   */ t=T2(w); SHIFT(w, 1) T(w) = ram[t]; break;
				case 0x95: /* STAk   */ t=T2(w); n=L(w); ram[t] = n; break;
				case 0x96: /* DEIk   */ t=T(w); SHIFT(w, 1) T(w) = DEI(t); break;
				case 0x97: /* DEOk   */ t=T(w); n=N(w); DEO(t, n); break;
				case 0x98: /* ADDk   */ t=T(w); n=N(w); SHIFT(w, 1) T(w) = n + t; break;
				case 0x99: /* SUBk   */ t=T(w); n=N(w); SHIFT(w, 1) T(w) = n - t; break;
				case 0x9a: /* MULk   */ t=T(w); n=N(w); SHIFT(w, 1) T(w) = n * t; break;
				case 0x9b: /* DIVk   */ t=T(w); n=N(w); SHIFT(w, 1) T(w) = t ? n / t : 0; break;
				case 0x9c: /* ANDk   */ t=T(w); n=N(w); SHIFT(w, 1) T(w) = n & t; break;
				case 0x9d: /* ORAk   */ t=T(w); n=N(w); SHIFT(w, 1) T(w) = n | t; break;
				case 0x9e: /* EORk   */ t=T(w); n=N(w); SHIFT(w, 1) T(w) = n ^ t; break;
				case 0x9f: /* SFTk   */ t=T(w); n=N(w); SHIFT(w, 1) T(w) = n >> (t & 0xf) << (t >> 4); break;
				case 0xa0: /* LIT2   */ SHIFT(w, 2) N(w) = ram[pc++]; T(w) = ram[pc++]; break;
				case 0xa1: /* INC2k  */ t=T2(w); SHIFT(w, 2) T2_(w, t + 1) break;
				case 0xa2: /* POP2k  */ break;
				case 0xa3: /* NIP2k  */ t=T2(w); SHIFT(w, 2) T2_(w, t) break;
				case 0xa4: /* SWP2k  */ t=T2(w); n=N2(w); SHIFT(w, 4) T2_(w, n) N2_(w, t) break;
				case 0xa5: /* ROT2k  */ t=T2(w); n=N2(w); l=L2(w); SHIFT(w, 6) T2_(w, l) N2_(w, t) L2_(w, n) break;
				case 0xa6: /* DUP2k  */ t=T2(w); SHIFT(w, 4) T2_(w, t) N2_(w, t) break;
				case 0xa7: /* OVR2k  */ t=T2(w); n=N2(w); SHIFT(w, 6) T2_(w, n) N2_(w, t) L2_(w, n) break;
				case 0xa8: /* EQU2k  */ t=T2(w); n=N2(w); SHIFT(w, 1) T(w) = n == t; break;
				case 0xa9: /* NEQ2k  */ t=T2(w); n=N2(w); SHIFT(w, 1) T(w) = n != t; break;
				case 0xaa: /* GTH2k  */ t=T2(w); n=N2(w); SHIFT(w, 1) T(w) = n >  t; break;
				case 0xab: /* LTH2k  */ t=T2(w); n=N2(w); SHIFT(w, 1) T(w) = n <  t; break;
				case 0xac: /* JMP2k  */ t=T2(w); pc = t; break;
				case 0xad: /* JCN2k  */ t=T2(w); n=L(w); if(n) pc = t; break;
				case 0xae: /* JSR2k  */ t=T2(w); SHIFT(r, 2) T2_(r, pc) pc = t; break;
				case 0xaf: /* STH2k  */ t=T2(w); SHIFT(r, 2) T2_(r, t) break;
				case 0xb0: /* LDZ2k  */ t=T(w); SHIFT(w, 2) N(w) = ram[t++]; T(w) = ram[(Uint8)t]; break;
				case 0xb1: /* STZ2k  */ t=T(w); n=H2(w); ram[t++] = n >> 8; ram[(Uint8)t] = n; break;
				case 0xb2: /* LDR2k  */ t=T(w); SHIFT(w, 2) q = pc + (Sint8)t; N(w) = ram[q++]; T(w) = ram[q]; break;
				case 0xb3: /* STR2k  */ t=T(w); n=H2(w); q = pc + (Sint8)t; ram[q++] = n >> 8; ram[q] = n; break;
				case 0xb4: /* LDA2k  */ t=T2(w); SHIFT(w, 2) N(w) = ram[t++]; T(w) = ram[t]; break;
				case 0xb5: /* STA2k  */ t=T2(w); n=N2(w); ram[t++] = n >> 8; ram[t] = n; break;
				case 0xb6: /* DEI2k  */ t=T(w); SHIFT(w, 2) N(w) = DEI(t); T(w) = DEI((t + 1)); break;
				case 0xb7: /* DEO2k  */ t=T(w); n=N(w); l=L(w); DEO(t, l); DEO((t + 1), n); break;
				case 0xb8: /* ADD2k  */ t=T2(w); n=N2(w); SHIFT(w, 2) T2_(w, n + t) break;
				case 0xb9: /* SUB2k  */ t=T2(w); n=N2(w); SHIFT(w, 2) T2_(w, n - t) break;
				case 0xba: /* MUL2k  */ t=T2(w); n=N2(w); SHIFT(w, 2) T2_(w, n * t) break;
				case 0xbb: /* DIV2k  */ t=T2(w); n=N2(w); SHIFT(w, 2) T2_(w, t ? n / t : 0) break;
				case 0xbc: /* AND2k  */ t=T2(w); n=N2(w); SHIFT(w, 2) T2_(w, n & t) break;
				case 0xbd: /* ORA2k  */ t=T2(w); n=N2(w); SHIFT(w, 2) T2_(w, n | t) break;
				case 0xbe: /* EOR2k  */ t=T2(w); n=N2(w); SHIFT(w, 2) T2_(w, n ^ t) break;
				case 0xbf: /* SFT2k  */ t=T(w); n=H2(w); SHIFT(w, 2) T2_(w, n >> (t & 0xf) << (t >> 4)) break;
				case 0xc0: /* LITr   */ SHIFT(r, 1) T(r) = ram[pc++]; break;
				case 0xc1: /* INCkr  */ t=T(r); SHIFT(r, 1) T(r) = t + 1; break;
				case 0xc2: /* POPkr  */ break;
				case 0xc3: /* NIPkr  */ t=T(r); SHIFT(r, 1) T(r) = t; break;
				case 0xc4: /* SWPkr  */ t=T(r); n=N(r); SHIFT(r, 2) T(r)=n; N(r)=t; break;
				case 0xc5: /* ROTkr  */ t=T(r); n=N(r); l=L(r); SHIFT(r, 3) T(r)=l; N(r)=t; L(r)=n; break;
				case 0xc6: /* DUPkr  */ t=T(r); SHIFT(r, 2) T(r)=t; N(r)=t; break;
				case 0xc7: /* OVRkr  */ t=T(r); n=N(r); SHIFT(r, 3) T(r)=n; N(r)=t; L(r)=n; break;
				case 0xc8: /* EQUkr  */ t=T(r); n=N(r); SHIFT(r, 1) T(r) = n == t; break;
				case 0xc9: /* NEQkr  */ t=T(r); n=N(r); SHIFT(r, 1) T(r) = n != t; break;
				case 0xca: /* GTHkr  */ t=T(r); n=N(r); SHIFT(r, 1) T(r) = n >  t; break;
				case 0xcb: /* LTHkr  */ t=T(r); n=N(r); SHIFT(r, 1) T(r) = n <  t; break;
				case 0xcc: /* JMPkr  */ t=T(r); pc += (Sint8)t; break;
				case 0xcd: /* JCNkr  */ t=T(r); n=N(r); if(n) pc += (Sint8)t; break;
				case 0xce: /* JSRkr  */ t=T(r); SHIFT(w, 2) T2_(w, pc) pc += (Sint8)t; break;
				case 0xcf: /* STHkr  */ t=T(r); SHIFT(w, 1) T(w)=t; break;
				case 0xd0: /* LDZkr  */ t=T(r); SHIFT(r, 1) T(r) = ram[t]; break;
				case 0xd1: /* STZkr  */ t=T(r); n=N(r); ram[t] = n; break;
				case 0xd2: /* LDRkr  */ t=T(r); SHIFT(r, 1) q = pc + (Sint8)t; T(r) = ram[q]; break;
				case 0xd3: /* STRkr  */ t=T(r); n=N(r); q = pc + (Sint8)t; ram[q] = n; break;
				case 0xd4: /* LDAkr  */ t=T2(r); SHIFT(r, 1) T(r) = ram[t]; break;
				case 0xd5: /* STAkr  */ t=T2(r); n=L(r); ram[t] = n; break;
				case 0xd6: /* DEIkr  */ t=T(r); SHIFT(r, 1) T(r) = DEI(t); break;
				case 0xd7: /* DEOkr  */ t=T(r); n=N(r); DEO(t, n); break;
				case 0xd8: /* ADDkr  */ t=T(r); n=N(r); SHIFT(r, 1) T(r) = n + t; break;
				case 0xd9: /* SUBkr  */ t=T(r); n=N(r); SHIFT(r, 1) T(r) = n - t; break;
				case 0xda: /* MULkr  */ t=T(r); n=N(r); SHIFT(r, 1) T(r) = n * t; break;
				case 0xdb: /* DIVkr  */ t=T(r); n=N(r); SHIFT(r, 1) T(r) = t ? n / t : 0; break;
				case 0xdc: /* ANDkr  */ t=T(r); n=N(r); SHIFT(r, 1) T(r) = n & t; break;
				case 0xdd: /* ORAkr  */ t=T(r); n=N(r); SHIFT(r, 1) T(r) = n | t; break;
				case 0xde: /* EORkr  */ t=T(r); n=N(r); SHIFT(r, 1) T(r) = n ^ t; break;
				case 0xdf: /* SFTkr  */ t=T(r); n=N(r); SHIFT(r, 1) T(r) = n >> (t & 0xf) << (t >> 4); break;
				case 0xe0: /* LIT2r  */ SHIFT(r, 2) N(r) = ram[pc++]; T(r) = ram[pc++]; break;
				case 0xe1: /* INC2kr */ t=T2(r); SHIFT(r, 2) T2_(r, t + 1) break;
				case 0xe2: /* POP2kr */ break;
				case 0xe3: /* NIP2kr */ t=T2(r); SHIFT(r, 2) T2_(r, t) break;
				case 0xe4: /* SWP2kr */ t=T2(r); n=N2(r); SHIFT(r, 4) T2_(r, n) N2_(r, t) break;
				case 0xe5: /* ROT2kr */ t=T2(r); n=N2(r); l=L2(r); SHIFT(r, 6) T2_(r, l) N2_(r, t) L2_(r, n) break;
				case 0xe6: /* DUP2kr */ t=T2(r); SHIFT(r, 4) T2_(r, t) N2_(r, t) break;
				case 0xe7: /* OVR2kr */ t=T2(r); n=N2(r); SHIFT(r, 6) T2_(r, n) N2_(r, t) L2_(r, n) break;
				case 0xe8: /* EQU2kr */ t=T2(r); n=N2(r); SHIFT(r, 1) T(r) = n == t; break;
				case 0xe9: /* NEQ2kr */ t=T2(r); n=N2(r); SHIFT(r, 1) T(r) = n != t; break;
				case 0xea: /* GTH2kr */ t=T2(r); n=N2(r); SHIFT(r, 1) T(r) = n >  t; break;
				case 0xeb: /* LTH2kr */ t=T2(r); n=N2(r); SHIFT(r, 1) T(r) = n <  t; break;
				case 0xec: /* JMP2kr */ t=T2(r); pc = t; break;
				case 0xed: /* JCN2kr */ t=T2(r); n=L(r); if(n) pc = t; break;
				case 0xee: /* JSR2kr */ t=T2(r); SHIFT(w, 2) T2_(w, pc) pc = t; break;
				case 0xef: /* STH2kr */ t=T2(r); SHIFT(w, 2) T2_(w, t) break;
				case 0xf0: /* LDZ2kr */ t=T(r); SHIFT(r, 2) N(r) = ram[t++]; T(r) = ram[(Uint8)t]; break;
				case 0xf1: /* STZ2kr */ t=T(r); n=H2(r); ram[t++] = n >> 8; ram[(Uint8)t] = n; break;
				case 0xf2: /* LDR2kr */ t=T(r); SHIFT(r, 2) q = pc + (Sint8)t; N(r) = ram[q++]; T(r) = ram[q]; break;
				case 0xf3: /* STR2kr */ t=T(r); n=H2(r); q = pc + (Sint8)t; ram[q++] = n >> 8; ram[q] = n; break;
				case 0xf4: /* LDA2kr */ t=T2(r); SHIFT(r, 2) N(r) = ram[t++]; T(r) = ram[t]; break;
				case 0xf5: /* STA2kr */ t=T2(r); n=N2(r); ram[t++] = n >> 8; ram[t] = n; break;
				case 0xf6: /* DEI2kr */ t=T(r); SHIFT(r, 2) N(r) = DEI(t); T(r) = DEI((t + 1)); break;
				case 0xf7: /* DEO2kr */ t=T(r); n=N(r); l=L(r); DEO(t, l); DEO((t + 1), n); break;
				case 0xf8: /* ADD2kr */ t=T2(r); n=N2(r); SHIFT(r, 2) T2_(r, n + t) break;
				case 0xf9: /* SUB2kr */ t=T2(r); n=N2(r); SHIFT(r, 2) T2_(r, n - t) break;
				case 0xfa: /* MUL2kr */ t=T2(r); n=N2(r); SHIFT(r, 2) T2_(r, n * t) break;
				case 0xfb: /* DIV2kr */ t=T2(r); n=N2(r); SHIFT(r, 2) T2_(r, t ? n / t : 0) break;
				case 0xfc: /* AND2kr */ t=T2(r); n=N2(r); SHIFT(r, 2) T2_(r, n & t) break;
				case 0xfd: /* ORA2kr */ t=T2(r); n=N2(r); SHIFT(r, 2) T2_(r, n | t) break;
				case 0xfe: /* EOR2kr */ t=T2(r); n=N2(r); SHIFT(r, 2) T2_(r, n ^ t) break;
				case 0xff: /* SFT2kr */ t=T(r); n=H2(r); SHIFT(r, 2) T2_(r, n >> (t & 0xf) << (t >> 4)) break;
			}
		#endif
	}
}


int
uxn_boot(Uxn *u, Uint8 *ram)
{
	if(ram) {
		u->ram = ram;
		return 1;
	} else return 0;
}
