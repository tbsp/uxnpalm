#include <PalmOS.h>
#include "ui_IDs.h"
#include "uxn.h"
#include "uxnemu.h"
#include "devices/system.h"
#include "devices/screen.h"
#include "devices/controller.h"
#include "devices/mouse.h"
#include "devices/datetime.h"

#define	uxnAppID 'UxnV'
#define	uxnDBType 'ROMU'

#define SUPPORT 0x1c03 /* devices mask */

const Uint16 deo_mask[] = {0xff28, 0x0300, 0xc028, 0x8000, 0x8000, 0x8000, 0x8000, 0x0000, 0x0000, 0x0000, 0xa260, 0xa260, 0x0000, 0x0000, 0x0000, 0x0000};
const Uint16 dei_mask[] = {0x0000, 0x0000, 0x003c, 0x0014, 0x0014, 0x0014, 0x0014, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x07ff, 0x0000, 0x0000, 0x0000};

static const UInt32 KEY_MASK = keyBitHard1 | keyBitHard2 | keyBitHard3 | keyBitHard4 | keyBitPageUp | keyBitPageDown; /* Hardware buttons we care about */

/*
Copyright (c) 2021-2023 Devine Lu Linvega, Andrew Alderwick
Copyright (c) 2023 Dave VanEe

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

//#define SHOW_TPF // Ticks Per Frame

void* alloc(UInt32 qtty, UInt8 sz) {
    MemPtr p = MemPtrNew(sz*qtty);
    if(!p) SysFatalAlert("Failed to allocate Uxn RAM");
    MemSet(p, sz*qtty, 0);
    return p;
}

static void *GetObjectPtr(UInt16 objectID)
{
    FormType *form = FrmGetActiveForm();
    return (FrmGetObjectPtr(form, FrmGetObjectIndex(form, objectID)));
}

static void InitializeROMList()
{
  UInt16            numROMs;
  char            **romNames;
  UInt16            cardNo;
  UInt32            dbType;
  DmSearchStateType searchState;
  LocalID           dbID;
  char              name[33];
  Boolean           first;

  /* First count number of ROMs */
  numROMs = 0;
  first = true;
  while (!DmGetNextDatabaseByTypeCreator(first, &searchState, NULL, uxnAppID, false, &cardNo, &dbID))
  {
    first = false;
    DmDatabaseInfo(cardNo, dbID, name, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &dbType, NULL);
    if (dbType == uxnDBType)
      numROMs++;
  }
  romNames = MemPtrNew(numROMs * sizeof(Char *));

  numROMs = 0;
  first = true;
  while (!DmGetNextDatabaseByTypeCreator(first, &searchState, NULL, uxnAppID, false, &cardNo, &dbID))
  {
      first = false;
      DmDatabaseInfo(cardNo, dbID, name, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &dbType, NULL);
      if (dbType == uxnDBType)
      {
        romNames[numROMs] = MemPtrNew(32);
        StrCopy(romNames[numROMs], name);
        numROMs++;

      }
  }
  LstSetListChoices(GetObjectPtr(listID_roms), romNames, numROMs);

  // Store pointer for cleanup
  SharedVariables *sharedVars = (SharedVariables*)globalsSlotVal(GLOBALS_SLOT_SHARED_VARS);
  sharedVars->romNames = romNames;
  sharedVars->numROMs = numROMs;
 }

static void
redraw(void)
{
  UxnScreen *uxn_screen = (UxnScreen*)globalsSlotVal(GLOBALS_SLOT_SCREEN_DEVICE);

  BitmapType *bitmap = screen_draw_changed(uxn_screen);
  WinDrawBitmap(bitmap, uxn_screen->x1, uxn_screen->y1);
 	uxn_screen->x1 = uxn_screen->y1 = 0xffff;
	uxn_screen->x2 = uxn_screen->y2 = 0;
  BmpDelete(bitmap);

  #ifdef SHOW_TPF
  SharedVariables *sharedVars = (SharedVariables*)globalsSlotVal(GLOBALS_SLOT_SHARED_VARS);
  int tpf = TimGetTicks() - sharedVars->ticksOfLastFrame;

  char tpfText[16];
  StrIToA(tpfText, tpf);
  WinDrawChars(tpfText, StrLen(tpfText), 0, 0);

  sharedVars->ticksOfLastFrame = TimGetTicks();
  #endif
}

Uint8
emu_dei(Uxn *u, Uint8 addr)
{
	Uint8 p = addr & 0x0f, d = addr & 0xf0;
	switch(d) {
	case 0x20: return screen_dei(&u->dev[d], p);
	//case 0xa0: return file_dei(0, &u->dev[d], p);
	//case 0xb0: return file_dei(1, &u->dev[d], p);
	case 0xc0: return datetime_dei(&u->dev[d], p);
	}
	return u->dev[addr];
}

void
emu_deo(Uxn *u, Uint8 addr)
{
	Uint8 p = addr & 0x0f, d = addr & 0xf0;
  UxnScreen *uxn_screen;
	switch(d) {
	case 0x00:
		system_deo(u, &u->dev[d], p);
		if(p > 0x7 && p < 0xe)
	    uxn_screen = (UxnScreen*)globalsSlotVal(GLOBALS_SLOT_SCREEN_DEVICE);
			screen_palette(uxn_screen, &u->dev[0x8]);
		break;
	case 0x20: screen_deo(u->ram, &u->dev[d], p); break;
	//case 0xa0: file_deo(0, u->ram, &u->dev[d], p); break;
	//case 0xb0: file_deo(1, u->ram, &u->dev[d], p); break;
	}
}

static int
InitializeEmulator(char *romName) {
  //MemSemaphoreReserve(true);

  // Initialize and store the global uxn pointer
  struct Uxn *u;
  u = (Uxn *)MemPtrNew(sizeof(Uxn));
  if(!u) SysFatalAlert("Failed to allocate Uxn");
  MemSet(u, sizeof(Uxn), 0);
  *globalsSlotPtr(GLOBALS_SLOT_UXN_CORE) = u;

  // Initialize and store the global screen pointer
  UxnScreen *uxn_screen = MemPtrNew(sizeof(UxnScreen));
  if(!u) SysFatalAlert("Failed to allocate screen");
  *globalsSlotPtr(GLOBALS_SLOT_SCREEN_DEVICE) = uxn_screen;
  screen_init(uxn_screen);
  screen_resize(uxn_screen, WIDTH, HEIGHT);

  // This boot/load/eval is 'start' in ref uxnemu
  if(!uxn_boot(u, (Uint8 *)alloc(MAX_RAM_ADDR, sizeof(Uint8))))
    return false;
  if(!system_load(u, romName))
    return false;

  uxn_eval(u, PAGE_PROGRAM);

  // Do this once here since it should never change
  WinSetDrawWindow(WinGetDisplayWindow());

  // Start regular update events
  SharedVariables *sharedVars = (SharedVariables*)globalsSlotVal(GLOBALS_SLOT_SHARED_VARS);
  sharedVars->timeForNextFrame = TimGetTicks() + sharedVars->timeInterval;

  //MemSemaphoreRelease(true);
  return true;
}

static Boolean AppHandleEvent(EventPtr event)
{
  UInt16 i;
  FormPtr pfrm;
  Int32 formId;
  Boolean handled;
  char romName[33];

  switch(event->eType) {
    case frmLoadEvent:
      formId = event->data.frmLoad.formID;
      pfrm = FrmInitForm(formId);
      FrmSetActiveForm(pfrm);
      switch(formId)
      {
        case formID_load:
          InitializeROMList();
          break;
        case formID_run:
          if(!InitializeEmulator(romName))
            return false;
          break;
        default:
          break;
      }
      break;
    case frmOpenEvent:
      formId = event->data.frmLoad.formID;
      switch(formId)
      {
        case formID_load:
        pfrm = FrmGetActiveForm();
        FrmDrawForm(pfrm);
        break;
      }
    case menuEvent:
      break;
    case ctlSelectEvent:
      switch (event->data.ctlSelect.controlID)
      {
        case buttonID_run:
          i = LstGetSelection(GetObjectPtr(listID_roms));
          StrCopy(romName, LstGetSelectionText(GetObjectPtr(listID_roms), i));

          FrmGotoForm(formID_run);

          break;
      }
      break;
    case appStopEvent:
      break;
    default:
      if(FrmGetActiveForm())
        FrmHandleEvent(FrmGetActiveForm(), event);
  }
  handled = true;
  return handled;
}

static void MakeSharedVariables(void)
{
	SharedVariables *sharedVars = (SharedVariables *)MemPtrNew(sizeof(SharedVariables));
  if(!sharedVars) SysFatalAlert("Failed to allocate shared variables");

	MemSet(sharedVars, sizeof(SharedVariables), 0);

	sharedVars->timeInterval = SysTicksPerSecond() / 50; // Since SysTicksPerSecond returns 100, we can only cleanly hit 50Hz, so do that for now
	sharedVars->timeForNextFrame = evtWaitForever;

	*globalsSlotPtr(GLOBALS_SLOT_SHARED_VARS) = sharedVars;
}

static void AppStart()
{
	FrmGotoForm(formID_load);
  MakeSharedVariables();
  KeySetMask(keyBitPower | keyBitCradle | keyBitAntenna | keyBitContrast);
}

int timeLeft(void) {
  int timeLeft = evtWaitForever;

  SharedVariables *sharedVars = (SharedVariables*)globalsSlotVal(GLOBALS_SLOT_SHARED_VARS);

  if(sharedVars->timeForNextFrame != evtWaitForever) {
    timeLeft = sharedVars->timeForNextFrame - TimGetTicks();
    if(timeLeft <= 0) timeLeft = 0;
  }

  return timeLeft;
}

static UInt8
get_button_state(UInt32 keyState)
{
  UInt8 buttonState = 0;
  if(keyState & keyBitHard4)    buttonState |= 0x01;  // A
  if(keyState & keyBitHard3)    buttonState |= 0x02;  // B
  if(keyState & keyBitPageUp)   buttonState |= 0x10;  // up
  if(keyState & keyBitPageDown) buttonState |= 0x20;  // down
  if(keyState & keyBitHard1)    buttonState |= 0x40;  // left
  if(keyState & keyBitHard2)    buttonState |= 0x80;  // right

  // start/select use soft buttons (find/calc)

	return buttonState;
}

Boolean preprocessUxn(EventPtr event) {
  
  int chr;
  UxnScreen *uxn_screen;
  Uxn *u = (Uxn*)globalsSlotVal(GLOBALS_SLOT_UXN_CORE);
  SharedVariables *sharedVars = (SharedVariables*)globalsSlotVal(GLOBALS_SLOT_SHARED_VARS);

  if(u) {
    if(!timeLeft()) {

      Uint8 *vector_addr = &u->dev[0x20];
      Uint16 screen_vector;
      screen_vector = PEEK2(vector_addr);

      uxn_eval(u, screen_vector);

      uxn_screen = (UxnScreen*)globalsSlotVal(GLOBALS_SLOT_SCREEN_DEVICE);
      if(uxn_screen->x2) redraw();

      sharedVars->timeForNextFrame = TimGetTicks() + sharedVars->timeInterval;
    }

    /* Controller - gamepad */
    // Note: No keyUpEvent exists, so we manually track changes to the buttons we care about
    UInt32 keyState = KeyCurrentState() & KEY_MASK;
    if(keyState != sharedVars->lastKeyState) {
      controller_buttons(u, &u->dev[0x80], get_button_state(keyState));
      sharedVars->lastKeyState = keyState;
    }

    switch(event->eType) {
      /* Controller - keyboard */
      case keyDownEvent:
        chr = event->data.keyDown.chr;
        if(chr < 0x80) {
          controller_key(u, &u->dev[0x80], chr);
          return true;
        }
        break;

      /* Mouse */
      case penMoveEvent:
        uxn_screen = (UxnScreen*)globalsSlotVal(GLOBALS_SLOT_SCREEN_DEVICE);
        mouse_pos(u, &u->dev[0x90], clamp(event->screenX, 0, uxn_screen->width - 1), clamp(event->screenY, 0, uxn_screen->height - 1), 1);
        return true;
        break;
      case penDownEvent:
        if(event->screenY < 160) {
          mouse_pos(u, &u->dev[0x90], clamp(event->screenX, 0, uxn_screen->width - 1), clamp(event->screenY, 0, uxn_screen->height - 1), 0);
          mouse_down(u, &u->dev[0x90], 0x1);
          return true;
        }
        break;
      case penUpEvent:
        if(event->screenY < 160) {
          mouse_pos(u, &u->dev[0x90], clamp(event->screenX, 0, uxn_screen->width - 1), clamp(event->screenY, 0, uxn_screen->height - 1), 0);
          mouse_up(u, &u->dev[0x90], 0x1);
          return true;
        }
        break;
      default: break;
    }
        
  }
  return false;
}

static void AppEventLoop(void)
{
  EventType event;
  UInt16 error;

  do {
    EvtGetEvent(&event, timeLeft());

    if(preprocessUxn(&event))
      continue;
    if(SysHandleEvent(&event))
      continue;
    if(MenuHandleEvent((void *)0, &event, &error))
      continue;
    if(AppHandleEvent(&event))
      continue;

  } while (event.eType != appStopEvent);
}

static void AppStop()
{
  FrmCloseAllForms();

  SharedVariables *sharedVars = (SharedVariables*)globalsSlotVal(GLOBALS_SLOT_SHARED_VARS);

  if(sharedVars) {
    if(sharedVars->numROMs > 0)
      for(int i=0; i<sharedVars->numROMs; i++)
        MemPtrFree(sharedVars->romNames[i]);
    MemPtrFree(sharedVars->romNames);
    MemPtrFree(sharedVars);
  }

  Uxn *u = (Uxn*)globalsSlotVal(GLOBALS_SLOT_UXN_CORE);
  if(u) {
    if(u->ram) MemPtrFree(u->ram);
    MemPtrFree(u);
  }

  UxnScreen *uxn_screen = (UxnScreen*)globalsSlotVal(GLOBALS_SLOT_SCREEN_DEVICE);
  if(uxn_screen) {
    if(uxn_screen->bg) MemPtrFree(uxn_screen->bg);
    if(uxn_screen->fg) MemPtrFree(uxn_screen->fg);
    MemPtrFree(uxn_screen);
  }
}

UInt32 PilotMain(UInt16 cmd, void *cmdPBP, UInt16 launchFlags) {

	if (sysAppLaunchCmdNormalLaunch == cmd) {
		AppStart();
		AppEventLoop();
    AppStop();
	}

	return 0;
}

UInt32 __attribute__((section(".vectors"))) __Startup__(void)
{
	SysAppInfoPtr appInfoP;
	void *prevGlobalsP;
	void *globalsP;
	UInt32 ret;
	
	SysAppStartup(&appInfoP, &prevGlobalsP, &globalsP);
	ret = PilotMain(appInfoP->cmd, appInfoP->cmdPBP, appInfoP->launchFlags);
	SysAppExit(appInfoP, prevGlobalsP, globalsP);
	
	return ret;
}
