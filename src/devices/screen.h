/*
Copyright (c) 2021 Devine Lu Linvega
Copyright (c) 2021 Andrew Alderwick

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

#define FIXED_SIZE 1

#define WIDTH 160
#define HEIGHT 160

#define PADDING 7

#define FULL_WIDTH (WIDTH + PADDING * 2) 
#define FULL_HEIGHT (HEIGHT + PADDING * 2)

typedef struct UxnScreen {
	Uint16 width, height, x1, y1, x2, y2;
	Uint8 *fg, *bg;
	RGBColorType colors[4];
	Uint8 paletteChanged;
} UxnScreen;

extern UxnScreen uxn_screen;

void screen_change(UxnScreen *p, Uint16 x1, Uint16 y1, Uint16 x2, Uint16 y2);
void screen_palette(UxnScreen *p, Uint8 *addr);
void screen_init(UxnScreen *p);
void screen_resize(UxnScreen *p, Uint16 width, Uint16 height);
void screen_fill(UxnScreen *p, Uint8 *layer, int x1, int y1, int x2, int y2, Uint8 color);
void screen_redraw(UxnScreen *p, BitmapType *bitmap);
BitmapType* screen_draw_changed(UxnScreen *p);

Uint8 screen_dei(Uint8 *d, Uint8 port);
void screen_deo(Uint8 *ram, Uint8 *d, Uint8 port);
int clamp(int val, int min, int max);
