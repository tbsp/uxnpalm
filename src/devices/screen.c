#include <stdlib.h>
#include <PalmOS.h>

#include "../uxn.h"
#include "../uxnemu.h"
#include "screen.h"

/*
Copyright (c) 2021-2023 Devine Lu Linvega, Andrew Alderwick

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

#define FAST_BLIT

#ifdef FAST_BLIT
	static const UInt16 dec_byte_flip_x[256] = {
		0x0000, 0x0001, 0x0004, 0x0005, 0x0010, 0x0011, 0x0014, 0x0015, 0x0040, 0x0041, 0x0044, 0x0045, 0x0050, 0x0050, 0x0054, 0x0055,
		0x0100, 0x0101, 0x0104, 0x0105,	0x0110, 0x0111, 0x0114, 0x0115,	0x0140, 0x0141, 0x0144, 0x0145,	0x0150, 0x0150, 0x0154, 0x0155,
		0x0400, 0x0401, 0x0404, 0x0405,	0x0410, 0x0411, 0x0414, 0x0415,	0x0440, 0x0441, 0x0444, 0x0445,	0x0450, 0x0450, 0x0454, 0x0455,
		0x0500, 0x0501, 0x0504, 0x0505,	0x0510, 0x0511, 0x0514, 0x0515,	0x0540, 0x0541, 0x0544, 0x0545,	0x0550, 0x0550, 0x0554, 0x0555,
		0x1000, 0x1001, 0x1004, 0x1005,	0x1010, 0x1011, 0x1014, 0x1015,	0x1040, 0x1041, 0x1044, 0x1045,	0x1050, 0x1050, 0x1054, 0x1055,
		0x1100, 0x1101, 0x1104, 0x1105,	0x1110, 0x1111, 0x1114, 0x1115,	0x1140, 0x1141, 0x1144, 0x1145,	0x1150, 0x1150, 0x1154, 0x1155,
		0x1400, 0x1401, 0x1404, 0x1405,	0x1410, 0x1411, 0x1414, 0x1415,	0x1440, 0x1441, 0x1444, 0x1445,	0x1450, 0x1450, 0x1454, 0x1455,
		0x1500, 0x1501, 0x1504, 0x1505,	0x1510, 0x1511, 0x1514, 0x1515,	0x1540, 0x1541, 0x1544, 0x1545,	0x1550, 0x1550, 0x1554, 0x1555,
		0x4000, 0x4001, 0x4004, 0x4005,	0x4010, 0x4011, 0x4014, 0x4015,	0x4040, 0x4041, 0x4044, 0x4045,	0x4050, 0x4050, 0x4054, 0x4055,
		0x4100, 0x4101, 0x4104, 0x4105,	0x4110, 0x4111, 0x4114, 0x4115,	0x4140, 0x4141, 0x4144, 0x4145,	0x4150, 0x4150, 0x4154, 0x4155,
		0x4400, 0x4401, 0x4404, 0x4405,	0x4410, 0x4411, 0x4414, 0x4415,	0x4440, 0x4441, 0x4444, 0x4445,	0x4450, 0x4450, 0x4454, 0x4455,
		0x4500, 0x4501, 0x4504, 0x4505,	0x4510, 0x4511, 0x4514, 0x4515,	0x4540, 0x4541, 0x4544, 0x4545,	0x4550, 0x4550, 0x4554, 0x4555,
		0x5000, 0x5001, 0x5004, 0x5005,	0x5010, 0x5011, 0x5014, 0x5015,	0x5040, 0x5041, 0x5044, 0x5045,	0x5050, 0x5050, 0x5054, 0x5055,
		0x5100, 0x5101, 0x5104, 0x5105,	0x5110, 0x5111, 0x5114, 0x5115,	0x5140, 0x5141, 0x5144, 0x5145,	0x5150, 0x5150, 0x5154, 0x5155,
		0x5400, 0x5401, 0x5404, 0x5405,	0x5410, 0x5411, 0x5414, 0x5415,	0x5440, 0x5441, 0x5444, 0x5445,	0x5450, 0x5450, 0x5454, 0x5455,
		0x5500, 0x5501, 0x5504, 0x5505,	0x5510, 0x5511, 0x5514, 0x5515,	0x5540, 0x5541, 0x5544, 0x5545,	0x5550, 0x5550, 0x5554, 0x5555
	};

	static const UInt16 dec_byte[256] = {
		0x0000, 0x4000, 0x1000, 0x5000, 0x0400, 0x4400, 0x1400, 0x5400, 0x0100, 0x4100, 0x1100, 0x5100, 0x0500, 0x4500, 0x1500, 0x5500,
		0x0040, 0x4040, 0x1040, 0x5040, 0x0440, 0x4440, 0x1440, 0x5440, 0x0140, 0x4140, 0x1140, 0x5140, 0x0540, 0x4540, 0x1540, 0x5540,
		0x0010, 0x4010, 0x1010, 0x5010, 0x0410, 0x4410, 0x1410, 0x5410, 0x0110, 0x4110, 0x1110, 0x5110, 0x0510, 0x4510, 0x1510, 0x5510,
		0x0050, 0x4050, 0x1050, 0x5050, 0x0450, 0x4450, 0x1450, 0x5450, 0x0150, 0x4150, 0x1150, 0x5150, 0x0550, 0x4550, 0x1550, 0x5550,
		0x0004, 0x4004, 0x1004, 0x5004, 0x0404, 0x4404, 0x1404, 0x5404, 0x0104, 0x4104, 0x1104, 0x5104, 0x0504, 0x4504, 0x1504, 0x5504,
		0x0044, 0x4044, 0x1044, 0x5044, 0x0444, 0x4444, 0x1444, 0x5444, 0x0144, 0x4144, 0x1144, 0x5144, 0x0544, 0x4544, 0x1544, 0x5544,
		0x0014, 0x4014, 0x1014, 0x5014, 0x0414, 0x4414, 0x1414, 0x5414, 0x0114, 0x4114, 0x1114, 0x5114, 0x0514, 0x4514, 0x1514, 0x5514,
		0x0054, 0x4054, 0x1054, 0x5054, 0x0454, 0x4454, 0x1454, 0x5454, 0x0154, 0x4154, 0x1154, 0x5154, 0x0554, 0x4554, 0x1554, 0x5554,
		0x0001, 0x4001, 0x1001, 0x5001, 0x0401, 0x4401, 0x1401, 0x5401, 0x0101, 0x4101, 0x1101, 0x5101, 0x0501, 0x4501, 0x1501, 0x5501,
		0x0041, 0x4041, 0x1041, 0x5041, 0x0441, 0x4441, 0x1441, 0x5441, 0x0141, 0x4141, 0x1141, 0x5141, 0x0541, 0x4541, 0x1541, 0x5541,
		0x0011, 0x4011, 0x1011, 0x5011, 0x0411, 0x4411, 0x1411, 0x5411, 0x0111, 0x4111, 0x1111, 0x5111, 0x0511, 0x4511, 0x1511, 0x5511,
		0x0051, 0x4051, 0x1051, 0x5051, 0x0451, 0x4451, 0x1451, 0x5451, 0x0151, 0x4151, 0x1151, 0x5151, 0x0551, 0x4551, 0x1551, 0x5551,
		0x0005, 0x4005, 0x1005, 0x5005, 0x0405, 0x4405, 0x1405, 0x5405, 0x0105, 0x4105, 0x1105, 0x5105, 0x0505, 0x4505, 0x1505, 0x5505,
		0x0045, 0x4045, 0x1045, 0x5045, 0x0445, 0x4445, 0x1445, 0x5445, 0x0145, 0x4145, 0x1145, 0x5145, 0x0545, 0x4545, 0x1545, 0x5545,
		0x0015, 0x4015, 0x1015, 0x5015, 0x0415, 0x4415, 0x1415, 0x5415, 0x0115, 0x4115, 0x1115, 0x5115, 0x0515, 0x4515, 0x1515, 0x5515,
		0x0055, 0x4055, 0x1055, 0x5055, 0x0455, 0x4455, 0x1455, 0x5455, 0x0155, 0x4155, 0x1155, 0x5155, 0x0555, 0x4555, 0x1555, 0x5555
	};
#endif

static const UInt8 blending[5][16] = {
	{0, 0, 0, 0, 1, 0, 1, 1, 2, 2, 0, 2, 3, 3, 3, 0},
	{0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3},
	{1, 2, 3, 1, 1, 2, 3, 1, 1, 2, 3, 1, 1, 2, 3, 1},
	{2, 3, 1, 2, 2, 3, 1, 2, 2, 3, 1, 2, 2, 3, 1, 2},
	{0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0}};

void
screen_change(UxnScreen *p, Uint16 x1, Uint16 y1, Uint16 x2, Uint16 y2)
{
	if(x1 > p->width && x2 > x1) return; // off-screen-right X, skip
	if(y1 > p->height && y2 > y1) return; // off-screen-bottom Y, skip
	if(x1 > x2) x1 = 0; // off-screen-left X, clamp
	if(y1 > y2) y1 = 0; // off-screen-top Y, clamp
	if(x1 < p->x1) p->x1 = x1; // expand changed region to include new values
	if(y1 < p->y1) p->y1 = y1;
	if(x2 > p->x2) p->x2 = x2;
	if(y2 > p->y2) p->y2 = y2;
}

void
screen_fill(UxnScreen *p, Uint8 *layer, int x1, int y1, int x2, int y2, Uint8 color)
{
	int y, width = p->width, height = p->height;
	int fillWidth = x2 > width ? width - x1 : x2 - x1;
	UInt8 *dst = &layer[x1 + PADDING + (y1 + PADDING) * FULL_WIDTH];
	for(y = y1; y < y2 && y < height; y++, dst += FULL_WIDTH)
		MemSet(dst, fillWidth, color);
}

static void
screen_blit(UxnScreen *p, Uint8 *layer, Uint16 x1, Uint16 y1, UInt8 *sprite, UInt8 color, UInt8 flipx, UInt8 flipy, UInt8 twobpp)
{
	// Skip drawing entirely out of bounds sprites entirely
	if(x1 >= WIDTH && x1 < 0xfff9) return;
	if(y1 >= HEIGHT && y1 < 0xfff9) return;

	int v, h, opaque = blending[4][color];

	// TODO: check if opaque is faster as %5 or LUT
	
	#ifndef FAST_BLIT
		if(opaque) {
			if(twobpp) {
				for(v = 0; v < 8; v++) {
					UInt16 c = sprite[v] | sprite[v + 8] << 8;
					UInt16 y = y1 + (flipy ? 7 - v : v);
					for(h = 7; h >= 0; --h, c >>= 1) {
						UInt8 ch = (c & 1) | ((c >> 7) & 2);
						UInt16 x = x1 + (flipx ? 7 - h : h);
						layer[x + PADDING + (y + PADDING) * FULL_WIDTH] = blending[ch][color];
					}
				}
			} else {
				for(v = 0; v < 8; v++) {
					UInt16 c = sprite[v];
					UInt16 y = y1 + (flipy ? 7 - v : v);
					for(h = 7; h >= 0; --h, c >>= 1) {
						UInt8 ch = (c & 1) | ((c >> 7) & 2);
						UInt16 x = x1 + (flipx ? 7 - h : h);
						layer[x + PADDING + (y + PADDING) * FULL_WIDTH] = blending[ch][color];
					}
				}
			}
		} else {
			if(twobpp) {
				for(v = 0; v < 8; v++) {
					UInt16 c = sprite[v] | sprite[v + 8] << 8;
					UInt16 y = y1 + (flipy ? 7 - v : v);
					for(h = 7; h >= 0; --h, c >>= 1) {
						UInt8 ch = (c & 1) | ((c >> 7) & 2);
						if(ch) {
							UInt16 x = x1 + (flipx ? 7 - h : h);
							layer[x + PADDING + (y + PADDING) * FULL_WIDTH] = blending[ch][color];
						}
					}
				}
			} else {
				for(v = 0; v < 8; v++) {
					UInt16 c = sprite[v];
					UInt16 y = y1 + (flipy ? 7 - v : v);
					for(h = 7; h >= 0; --h, c >>= 1) {
						UInt8 ch = (c & 1) | ((c >> 7) & 2);
						if(ch) {
							UInt16 x = x1 + (flipx ? 7 - h : h);
							layer[x + PADDING + (y + PADDING) * FULL_WIDTH] = blending[ch][color];
						}
					}
				}
			}
		}
	#else
		UInt16 c, width = p->width, height = p->height;
		const UInt16* lut = flipx ? dec_byte_flip_x : dec_byte;

		UInt8 *dst = &layer[x1 + PADDING + (y1 + PADDING) * FULL_WIDTH];

		if(twobpp) {
			if(color == 1) {
				if(!flipy) {
					for(v = 0; v < 8; v++, dst += FULL_WIDTH) {
						UInt8 ch1 = sprite[v];
						UInt8 ch2 = sprite[v + 8];
						c = lut[ch1] | (lut[ch2] << 1);
						for(h = 0; h < 8; h++, c >>= 2) dst[h] = c & 3;
					}
				} else {
					for(v = 0; v < 8; v++, dst += FULL_WIDTH) {
						UInt8 ch1 = sprite[7 - v];
						UInt8 ch2 = sprite[7 - v + 8];
						c = lut[ch1] | (lut[ch2] << 1);
						for(h = 0; h < 8; h++, c >>= 2) dst[h] = c & 3;
					}
				}
			} else if(opaque) {
				UInt8 c0 = blending[0][color], c1 = blending[1][color], c2 = blending[2][color], c3 = blending[3][color];
				if(!flipy) {
					for(v = 0; v < 8; v++, dst += FULL_WIDTH) {
						UInt8 ch1 = sprite[v];
						UInt8 ch2 = sprite[v + 8];
						c = lut[ch1] | (lut[ch2] << 1);
						UInt16 c1mask = (c & 0x5555);
						UInt16 c2mask = (c & 0xaaaa) >> 1;
						UInt16 c3mask = (c1mask & c2mask) * 3;
						c1mask &= ~c3mask;
						c2mask &= ~c3mask;
						c3mask = (c & c3mask) & 0x5555;
						UInt16 c0mask = ~(c1mask | c2mask | c3mask) & 0x5555;
						c = (c0 * c0mask) | (c1 * c1mask) | (c2 * c2mask) | (c3 * c3mask);
						for(h = 0; h < 8; h++, c >>= 2) dst[h] = c & 3;
					}
				} else {
					for(v = 0; v < 8; v++, dst += FULL_WIDTH) {
						UInt8 ch1 = sprite[7 - v];
						UInt8 ch2 = sprite[7 - v + 8];
						c = lut[ch1] | (lut[ch2] << 1);
						UInt16 c1mask = (c & 0x5555);
						UInt16 c2mask = (c & 0xaaaa) >> 1;
						UInt16 c3mask = (c1mask & c2mask) * 3;
						c1mask &= ~c3mask;
						c2mask &= ~c3mask;
						c3mask = (c & c3mask) & 0x5555;
						UInt16 c0mask = ~(c1mask | c2mask | c3mask) & 0x5555;
						c = (c0 * c0mask) | (c1 * c1mask) | (c2 * c2mask) | (c3 * c3mask);
						for(h = 0; h < 8; h++, c >>= 2) dst[h] = c & 3;
					}
				}
			} else {
				UInt8 c1 = blending[1][color], c2 = blending[2][color], c3 = blending[3][color];
				if(!flipy) {
					for(v = 0; v < 8; v++, dst += FULL_WIDTH) {
						UInt8 ch1 = sprite[v];
						UInt8 ch2 = sprite[v + 8];
						c = lut[ch1] | (lut[ch2] << 1);
						UInt16 c1mask = (c & 0x5555);
						UInt16 c2mask = (c & 0xaaaa) >> 1;
						UInt16 c3mask = (c1mask & c2mask) * 3;
						c1mask &= ~c3mask;
						c2mask &= ~c3mask;
						c3mask = (c & c3mask) & 0x5555;
						UInt16 mask = (c1mask | c2mask | c3mask) * 0x5;
						c = (c1 * c1mask) | (c2 * c2mask) | (c3 * c3mask);
						for(h = 0; h < 8; h++, c >>= 2, mask >>= 2) dst[h] = (dst[h] & ~mask) | (c & 3);
					}
				} else {
					for(v = 0; v < 8; v++, dst += FULL_WIDTH) {
						UInt8 ch1 = sprite[7 - v];
						UInt8 ch2 = sprite[7 - v + 8];
						c = lut[ch1] | (lut[ch2] << 1);
						UInt16 c1mask = (c & 0x5555);
						UInt16 c2mask = (c & 0xaaaa) >> 1;
						UInt16 c3mask = (c1mask & c2mask) * 3;
						c1mask &= ~c3mask;
						c2mask &= ~c3mask;
						c3mask = (c & c3mask) & 0x5555;
						UInt16 mask = (c1mask | c2mask | c3mask) * 0x5;
						c = (c1 * c1mask) | (c2 * c2mask) | (c3 * c3mask);
						for(h = 0; h < 8; h++, c >>= 2, mask >>= 2) dst[h] = (dst[h] & ~mask) | (c & 3);
					}
				}
			}
		// 1bpp
		} else {
			if(opaque) {
				if(!flipy) {
					for(v = 0; v < 8; v++, dst += FULL_WIDTH) {
						UInt8 ch1 = sprite[v];
						UInt16 color_1 = lut[ch1];
						UInt16 color_2 = (color_1 ^ 0xffff) & 0x5555;
						c = (color_1 * (color & 3)) | (color_2 * (color >> 2));
						for(h = 0; h < 8; h++, c >>= 2) dst[h] = c & 3;
					}
				} else {
					for(v = 0; v < 8; v++, dst += FULL_WIDTH) {
						UInt8 ch1 = sprite[7 - v];
						UInt16 color_1 = lut[ch1];
						UInt16 color_2 = (color_1 ^ 0xffff) & 0x5555;
						c = (color_1 * (color & 3)) | (color_2 * (color >> 2));
						for(h = 0; h < 8; h++, c >>= 2) dst[h] = c & 3;
					}
				}
			} else {
				if(!flipy) {
					for(v = 0; v < 8; v++, dst += FULL_WIDTH) {
						UInt8 ch1 = sprite[v];
						c = lut[ch1];
						UInt16 mask = c * 3;
						c *= color & 3;
						for(h = 0; h < 8; h++, c >>= 2, mask >>= 2) dst[h] = (dst[h] & ~mask) | (c & 3);
					}
				} else {
					for(v = 0; v < 8; v++, dst += FULL_WIDTH) {
						UInt8 ch1 = sprite[7 - v];
						c = lut[ch1];
						UInt16 mask = c * 3;
						c *= color & 3;
						for(h = 0; h < 8; h++, c >>= 2, mask >>= 2) dst[h] = (dst[h] & ~mask) | (c & 3);
					}
				}
			}
		}

	#endif
}

void
screen_palette(UxnScreen *p, UInt8 *addr)
{
	int i, shift;
	for(i = 0, shift = 4; i < 4; ++i, shift ^= 4) {
		UInt8
			r = (addr[0 + i / 2] >> shift) & 0x0f,
			g = (addr[2 + i / 2] >> shift) & 0x0f,
			b = (addr[4 + i / 2] >> shift) & 0x0f;
		p->colors[i] = (RGBColorType){0, r | r << 4, g | g << 4, b | b << 4};
	}
	p->paletteChanged = 1;
}

void screen_init(UxnScreen *p) {
	// The UxnScreen layers are internally always 160x160 with overflow regions for fast blitting
	if(p->bg) MemPtrFree(p->bg);
	if(p->fg) MemPtrFree(p->fg);

	MemPtr bg = MemPtrNew(FULL_WIDTH * FULL_HEIGHT * sizeof(UInt8));
	if(!bg) SysFatalAlert("Failed to allocate bg layer");
	MemPtr fg = MemPtrNew(FULL_WIDTH * FULL_HEIGHT * sizeof(UInt8));
	if(!fg) SysFatalAlert("Failed to allocate fg layer");

	p->bg = bg;
	p->fg = fg;
}

void
screen_resize(UxnScreen *p, Uint16 width, Uint16 height)
{
	p->width = width;
	p->height = height;
	screen_fill(p, p->bg, 0, 0, width, height, 0);
	screen_fill(p, p->fg, 0, 0, width, height, 0);
}

void
screen_redraw(UxnScreen *p, BitmapType *bitmap)
{
	UInt8 *pixels = BmpGetBits(bitmap);

	int i, j, o;
	Uint16 y, w = p->width, h = p->height;

	if(p->paletteChanged) {
		// Setting the palette is very slow on hardware (multiple frames), so
		//  do it once here instead of in the screen_palette function
		RGBColorType palette[16];
		for(i = 0; i < 16; i++)
			palette[i] = p->colors[(i >> 2) ? (i >> 2) : (i & 3)];
		
		WinPalette(winPaletteSet, 0, 16, palette);
		screen_change(p, 0, 0, w, h);
		p->paletteChanged = 0;
	}

	Uint8 *fg = p->fg, *bg = p->bg;
	Uint16 x1 = p->x1, y1 = p->y1;
	Uint16 x2 = p->x2 > w ? w : p->x2, y2 = p->y2 > h ? h : p->y2;

	for(y = y1; y < y2; y++)
		for(o = y * w, i = x1 + o, j = x2 + o; i < j; i++)
			pixels[i] = fg[i] << 2 | bg[i];

	p->x1 = p->y1 = 0xffff;
	p->x2 = p->y2 = 0;
}

BitmapType*
screen_draw_changed(UxnScreen *p)
{
	Err err;
	BitmapType *bitmap;

	int i, j, o, b = 0, stride_offset = 0;
	Uint16 y, w = p->width, h = p->height;

	if(p->paletteChanged) {
		// Setting the palette is very slow on hardware (multiple frames), so
		//  do it once here instead of in the screen_palette function
		RGBColorType palette[16];
		for(i = 0; i < 16; i++)
			palette[i] = p->colors[(i >> 2) ? (i >> 2) : (i & 3)];
		
		WinPalette(winPaletteSet, 0, 16, palette);
		screen_change(p, 0, 0, w, h);
		p->paletteChanged = 0;
	}

	Uint8 *fg = p->fg, *bg = p->bg;
	Uint16 x1 = p->x1, y1 = p->y1;
	Uint16 x2 = p->x2 > w ? w : p->x2, y2 = p->y2 > h ? h : p->y2;
	Uint16 xw = x2 - x1;

	// Bitmap stride must be a multiple of 2 bytes, so for 8bpp we need an extra index offset per row for odd-width bitmaps
	// The preferred approach is to call BmpGetDimensions to get the rowBytes and use that, or BmpGlueGetDimensions for PalmOS
	//  before 4.0. For now I'm gonna stick with this as PalmOSGlue isn't trivial to use with my toolchain and 3.5 is my main
	//  target.
	if(xw % 2) stride_offset++;

	bitmap = BmpCreate(xw, y2 - y1, 8, NULL, &err);
	if(!bitmap) SysFatalAlert("Failed to allocate changed bitmap");

	UInt8 *pixels = BmpGetBits(bitmap);
	for(y = y1; y < y2; y++, b += stride_offset)
		for(o = (y + PADDING) * FULL_WIDTH, i = x1 + PADDING + o, j = x2 + PADDING + o; i < j; i++, b++)
			pixels[b] = fg[i] << 2 | bg[i];

	return bitmap;
}

int
clamp(int val, int min, int max)
{
	return (val >= min) ? (val <= max) ? val : max : min;
}

/* IO */

UInt8
screen_dei(UInt8 *d, UInt8 port)
{
	struct UxnScreen *uxn_screen;
	uxn_screen = (struct UxnScreen*)globalsSlotVal(GLOBALS_SLOT_SCREEN_DEVICE);

	switch(port) {
	case 0x2: return uxn_screen->width >> 8;
	case 0x3: return uxn_screen->width;
	case 0x4: return uxn_screen->height >> 8;
	case 0x5: return uxn_screen->height;
	default: return d[port];
	}
}

void
screen_deo(UInt8 *ram, UInt8 *d, UInt8 port)
{
	UInt8 *port_width, *port_height, *port_x, *port_y, *port_addr;
	UxnScreen *uxn_screen = (UxnScreen*)globalsSlotVal(GLOBALS_SLOT_SCREEN_DEVICE);

	switch(port) {
	case 0x3:
		if(!FIXED_SIZE) {
			port_width = d + 0x2;
			screen_resize(uxn_screen, clamp(PEEK2(port_width), 1, 160), uxn_screen->height);
		}
		break;
	case 0x5:
		if(!FIXED_SIZE) {
			port_height = d + 0x4;
			screen_resize(uxn_screen, uxn_screen->width, clamp(PEEK2(port_height), 1, 160));
		}
		break;
	case 0xe: {
		UInt16 x, y;
		UInt8 ctrl = d[0xe];
		UInt8 color = ctrl & 0x3;
		UInt8 *layer = (ctrl & 0x40) ? uxn_screen->fg : uxn_screen->bg;
		port_x = d + 0x8;
		port_y = d + 0xa;
		x = PEEK2(port_x);
		y = PEEK2(port_y);
		if(ctrl & 0x80) {
			// fill mode
			Uint16 x2 = uxn_screen->width, y2 = uxn_screen->height;
			if(ctrl & 0x10) x2 = x, x = 0;
			if(ctrl & 0x20) y2 = y, y = 0;
			screen_fill(uxn_screen, layer, x, y, x2, y2, color);
			screen_change(uxn_screen, x, y, x2, y2);
		} else {
			// pixel mode
			if(x < uxn_screen->width && y < uxn_screen->height)
				layer[x + PADDING + (y + PADDING) * FULL_WIDTH] = color;
			screen_change(uxn_screen, x, y, x + 1, y + 1);
			if(d[0x6] & 0x01) POKE2(port_x, x + 1); /* auto x+1 */
			if(d[0x6] & 0x02) POKE2(port_y, y + 1); /* auto y+1 */
		}
		break;
	}
	case 0xf: {
		UInt16 x, y, dx, dy, dxy, dyx, addr, addr_incr;
		UInt8 ctrl = d[0xf];
		UInt8 move = d[0x6];
		UInt8 length = move >> 4;
		UInt8 i, twobpp = !!(d[0xf] & 0x80);
		UInt8 *layer = (d[0xf] & 0x40) ? uxn_screen->fg : uxn_screen->bg;
		UInt8 color = ctrl & 0xf;
		int flipx = (ctrl & 0x10), fx = flipx ? -1 : 1;
		int flipy = (ctrl & 0x20), fy = flipy ? -1 : 1;
		port_x = d + 0x8;
		port_y = d + 0xa;
		port_addr = d + 0xc;
		x = PEEK2(port_x), dx = (move & 0x1) << 3, dxy = dx * fy;
		y = PEEK2(port_y), dy = (move & 0x2) << 2, dyx = dy * fx;
		addr = PEEK2(port_addr), addr_incr = (move & 0x4) << (1 + twobpp);
		if(addr > 0x10000 - ((length + 1) << (3 + twobpp)))
			return;
		for(i = 0; i <= length; i++) {
			screen_blit(uxn_screen, layer, x + dyx * i, y + dxy * i, &ram[addr], color, flipx, flipy, twobpp);
			addr += addr_incr;
		}
		screen_change(uxn_screen, x, y, x + dyx * length + 8, y + dxy * length + 8);
		if(move & 0x1) POKE2(port_x, x + dx * fx); /* auto x+8 */
		if(move & 0x2) POKE2(port_y, y + dy * fy); /* auto y+8 */
		if(move & 0x4) POKE2(port_addr, addr);   /* auto addr+length */
		break;
	}
	}
}
