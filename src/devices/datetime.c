#include <PalmOS.h>

#include "../uxn.h"
#include "datetime.h"

/*
Copyright (c) 2021-2023 Devine Lu Linvega, Andrew Alderwick

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE.
*/

Uint8
datetime_dei(Uint8 *d, Uint8 port)
{
    UInt32 seconds = TimGetSeconds();
    DateTimeType datetime;
    TimSecondsToDateTime(seconds, &datetime);

    UInt8 yearDay = datetime.day - 1;
    for(int month=1; month < datetime.month; ++month) {
        yearDay += DaysInMonth(month, datetime.year);
    }

	switch(port) {
        case 0x0: return (datetime.year + 1900) >> 8;
        case 0x1: return (datetime.year + 1900);
        case 0x2: return datetime.month;
        case 0x3: return datetime.day;
        case 0x4: return datetime.hour;
        case 0x5: return datetime.minute;
        case 0x6: return datetime.second;
        case 0x7: return datetime.weekDay;
        case 0x8: return yearDay >> 8;
        case 0x9: return yearDay;
        case 0xa: return 0; // TODO: isdst
	    default: return d[port];
	}
}
