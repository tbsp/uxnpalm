#include <PalmOS.h>
#include "ui_IDs.h"
#include "uxn.h"
#include "uxncli.h"
#include "devices/system.h"

#define	uxnAppID 'UxnV'
#define	uxnDBType 'ROMU'

#define SUPPORT 0x1c03 /* devices mask */

const Uint16 deo_mask[] = {0xff28, 0x0300, 0xc028, 0x8000, 0x8000, 0x8000, 0x8000, 0x0000, 0x0000, 0x0000, 0xa260, 0xa260, 0x0000, 0x0000, 0x0000, 0x0000};
const Uint16 dei_mask[] = {0x0000, 0x0000, 0x003c, 0x0014, 0x0014, 0x0014, 0x0014, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x07ff, 0x0000, 0x0000, 0x0000};

void* alloc(UInt32 qtty, UInt8 sz) {
    MemPtr p = MemPtrNew(sz*qtty);
    MemSet(p, sz*qtty, 0);
    return p;
}

static void *GetObjectPtr(UInt16 objectID)
{
    FormType *form = FrmGetActiveForm();
    return (FrmGetObjectPtr(form, FrmGetObjectIndex(form, objectID)));
}

static void InitializeROMList()
{
  UInt16            numROMs;
  char            **romNames;
  UInt16            cardNo;
  UInt32            dbType;
  DmSearchStateType searchState;
  LocalID           dbID;
  char              name[33];
  Boolean           first;

  /* First count number of ROMs */
  numROMs = 0;
  first = true;
  while (!DmGetNextDatabaseByTypeCreator(first, &searchState, NULL, uxnAppID, false, &cardNo, &dbID))
  {
    first = false;
    DmDatabaseInfo(cardNo, dbID, name, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &dbType, NULL);
    if (dbType == uxnDBType)
      numROMs++;
  }
  romNames = MemPtrNew(numROMs * sizeof(Char *));

  numROMs = 0;
  first = true;
  while (!DmGetNextDatabaseByTypeCreator(first, &searchState, NULL, uxnAppID, false, &cardNo, &dbID))
  {
      first = false;
      DmDatabaseInfo(cardNo, dbID, name, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &dbType, NULL);
      if (dbType == uxnDBType)
      {
        romNames[numROMs] = MemPtrNew(32);
        StrCopy(romNames[numROMs], name);
        numROMs++;

      }
  }
  LstSetListChoices(GetObjectPtr(listID_roms), romNames, numROMs);

  // Store pointer for cleanup
  SharedVariables *sharedVars = (SharedVariables*)globalsSlotVal(GLOBALS_SLOT_SHARED_VARS);
  sharedVars->romNames = romNames;
  sharedVars->numROMs = numROMs;
 }

static void
console_deo(Uint8 *d, Uint8 port)
{
  if(port==0x8) {
    FieldType *fld = GetObjectPtr(fieldID_desc);

    void *ptr = FldGetTextPtr(fld);

    UInt8 len = StrLen(ptr);
    char *newStr = MemPtrNew(len + 2);
    StrCopy(newStr, ptr);
    newStr[len] = d[port];
    newStr[len + 1] = '\0';

    FldSetTextPtr(fld, newStr);
    MemPtrFree(ptr);

	  FldRecalculateField(fld, true);
    FldDrawField(fld);
  }
}

Uint8
emu_dei(Uxn *u, Uint8 addr)
{
	Uint8 p = addr & 0x0f, d = addr & 0xf0;
	switch(d) {
	//case 0xa0: return file_dei(0, &u->dev[d], p);
	//case 0xb0: return file_dei(1, &u->dev[d], p);
	//case 0xc0: return datetime_dei(&u->dev[d], p);
	}
	return u->dev[addr];
}

void
emu_deo(Uxn *u, Uint8 addr)
{
	Uint8 p = addr & 0x0f, d = addr & 0xf0;
	Uint16 mask = 0x1 << (d >> 4);
	switch(d) {
	//case 0x00: system_deo(u, &u->dev[d], p); break;
	case 0x10: console_deo(&u->dev[d], p); break;
	//case 0xa0: file_deo(0, u->ram, &u->dev[d], p); break;
	//case 0xb0: file_deo(1, u->ram, &u->dev[d], p); break;
	}
	if(p == 0x01 && !(SUPPORT & mask))
		ErrDisplay("Warning: Incompatible emulation, device ?");
}

static int
InitializeEmulator(char *romName) {

  FieldType *fld;
  char *consoleOutput;

  fld = GetObjectPtr(fieldID_desc);
  consoleOutput = MemPtrNew(1);
  FldSetTextPtr(fld, consoleOutput);

  // Initialize and store the global uxn pointer
  struct Uxn *u;
  u = (Uxn *)MemPtrNew(sizeof(Uxn));
  if(!u) SysFatalAlert("Failed to allocate Uxn");
  MemSet(u, sizeof(Uxn), 0);
  *globalsSlotPtr(GLOBALS_SLOT_UXN_CORE) = u;

  // This boot/load/eval is 'start' in ref uxnemu
  if(!uxn_boot(u, (Uint8 *)alloc(MAX_RAM_ADDR, sizeof(Uint8))))
    return false;
  if(!system_load(u, romName))
    return false;
  uxn_eval(u, PAGE_PROGRAM);

  return true;
}

static Boolean AppHandleEvent(EventPtr event)
{
  UInt16 i;
  FormPtr pfrm;
  Int32 formId;
  Boolean handled;
  char romName[33];

  switch(event->eType) {
    case frmLoadEvent:
      formId = event->data.frmLoad.formID;
      pfrm = FrmInitForm(formId);
      FrmSetActiveForm(pfrm);
      switch(formId)
      {
        case formID_load:
          InitializeROMList();
          break;
        case formID_run:
          if(!InitializeEmulator(romName))
            return false;
          break;
        default:
          break;
      }
      break;
    case frmOpenEvent:
      pfrm = FrmGetActiveForm();
      FrmDrawForm(pfrm);
      break;
    case menuEvent:
      break;
    case ctlSelectEvent:
      switch (event->data.ctlSelect.controlID)
      {
        case buttonID_run:
          i = LstGetSelection(GetObjectPtr(listID_roms));
          StrCopy(romName, LstGetSelectionText(GetObjectPtr(listID_roms), i));

          FrmGotoForm(formID_run);

          break;
      }
      break;
    case appStopEvent:
      break;
    default:
      if(FrmGetActiveForm())
        FrmHandleEvent(FrmGetActiveForm(), event);
  }
  handled = true;
  return handled;
}

static void MakeSharedVariables(void)
{
	SharedVariables *sharedVars = (SharedVariables *)MemPtrNew(sizeof(SharedVariables));
  if(!sharedVars) SysFatalAlert("Failed to allocate shared variables");

	MemSet(sharedVars, sizeof(SharedVariables), 0);

	*globalsSlotPtr(GLOBALS_SLOT_SHARED_VARS) = sharedVars;
}

static void AppStart()
{
	FrmGotoForm(formID_load);
  MakeSharedVariables();
}

static void AppEventLoop(void)
{
  EventType event;
  UInt16 error;

  do {
    EvtGetEvent(&event, evtWaitForever);

    if(SysHandleEvent(&event))
      continue;
    if(MenuHandleEvent((void *)0, &event, &error))
      continue;
    if(AppHandleEvent(&event))
      continue;

  } while (event.eType != appStopEvent);
}

static void AppStop()
{
  FieldType *fld = GetObjectPtr(fieldID_desc);
  void *ptr = FldGetTextPtr(fld);
  if(ptr) MemPtrFree(ptr);

  FrmCloseAllForms();

  SharedVariables *sharedVars = (SharedVariables*)globalsSlotVal(GLOBALS_SLOT_SHARED_VARS);

  if(sharedVars) {
    if(sharedVars->numROMs > 0)
      for(int i=0; i<sharedVars->numROMs; i++)
        MemPtrFree(sharedVars->romNames[i]);
    MemPtrFree(sharedVars->romNames);
    MemPtrFree(sharedVars);
  }

  Uxn *u = (Uxn*)globalsSlotVal(GLOBALS_SLOT_UXN_CORE);
  if(u) {
    if(u->ram) MemPtrFree(u->ram);
    MemPtrFree(u);
  }
}

UInt32 PilotMain(UInt16 cmd, void *cmdPBP, UInt16 launchFlags) {

	if (sysAppLaunchCmdNormalLaunch == cmd) {
		AppStart();
		AppEventLoop();
    AppStop();
	}

	return 0;
}

UInt32 __attribute__((section(".vectors"))) __Startup__(void)
{
	SysAppInfoPtr appInfoP;
	void *prevGlobalsP;
	void *globalsP;
	UInt32 ret;
	
	SysAppStartup(&appInfoP, &prevGlobalsP, &globalsP);
	ret = PilotMain(appInfoP->cmd, appInfoP->cmdPBP, appInfoP->launchFlags);
	SysAppExit(appInfoP, prevGlobalsP, globalsP);
	
	return ret;
}
