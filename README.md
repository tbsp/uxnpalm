# Uxn/Varvara VM for PalmOS

This is an early attempt at implementing the [Uxn virtual machine](https://100r.co/site/uxn.html) with [Varvara](https://wiki.xxiivv.com/site/varvara.html) devices for PalmOS. Current testing is being done with a Palm IIIc.

The screen device is close to parity with the current device specification, with sprite flipping not handling the auto byte properly. It's very slow for now! Other devices (controller, mouse, file) are currently not implemented.
